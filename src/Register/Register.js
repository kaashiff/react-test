import React, {Component} from "react";
import {Button, FormGroup, FormControl, FormLabel} from "react-bootstrap";
import "./Register.css";
import axios from "axios";
import {Redirect} from "react-router";
import auth from "../auth";
import {GoogleLogin} from "react-google-login";

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.responseGoogle = this.responseGoogle.bind(this);
        this.state = {
            email: "",
            password: "",
            confirmPassword: "",
            errorMsg: "",
            redirectToHome: auth.isLoggedIn()
        };
    }

    validateForm() {
        return  this.state.email.length > 0 &&
            this.state.password.length > 0 &&
            this.state.password === this.state.confirmPassword
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    responseGoogle(response) {
        if (response.error !== undefined) {
            alert(response.error);
        } else {
            localStorage.setItem('token', response.code);
            this.setState({
                redirectToHome: true,
            });
        }
    }

    handleSubmit = event => {
        event.preventDefault();
        this.setState({
            errorMsg:'',
        });
        axios.post('https://reqres.in/api/register', {email: this.state.email, password: this.state.password})
            .then(res => {
                localStorage.setItem('token', res.data.token);
                this.setState({
                    redirectToHome: true,
                });
            }).catch(error => {
            const errorMsg = error.response.data.error;
            this.setState({
                errorMsg,
            });
        })
    }

    render() {

        if (this.state.redirectToHome === true) {
            return <Redirect to='/'/>
        }

        return (
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bssize="large">
                        <FormLabel>Email</FormLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bssize="large">
                        <FormLabel>Password</FormLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <FormGroup controlId="confirmPassword" bssize="large">
                        <FormLabel>Confirm Password</FormLabel>
                        <FormControl
                            value={this.state.confirmPassword}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <FormLabel style={{color:'red'}}>{this.state.errorMsg}</FormLabel>
                    <Button
                        block
                        bssize="large"
                        disabled={!this.validateForm()}
                        type="submit">
                        Login
                    </Button>

                </form>
                <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '10px'}}>
                    <GoogleLogin
                        style={{width: '300px'}}
                        clientId="261836923984-qidalqiq53rhl59hrgqjb35o87delol2.apps.googleusercontent.com"
                        buttonText="Signup"
                        onSuccess={this.responseGoogle}
                        onFailure={this.responseGoogle}
                        cookiePolicy={'single_host_origin'}
                        responseType={'code'}
                    />
                </div>
                <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '10px'}}>
                    <a href="/login"> Already have an account? </a>
                </div>
            </div>

        );
    }
}
