import React, {Component} from 'react';

class Dashboard extends Component {
    constructor(props) {
        super(props);
    }

    updateWidth = () => {

        const w = window,
            d = document,
            documentElement = d.documentElement,
            body = d.getElementsByTagName('body')[0],
            width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        this.setState({width});
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateWidth);
    }

    componentWillMount() {
        this.updateWidth();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateWidth);
    }

    render() {
        return (
            <div className="container">
                {
                    this.state.width > 600 ?
                        <div className="row">
                            <div className="col-8">
                                <div className="row">
                                    <div className="box">A</div>
                                    <div className="box">B</div>
                                </div>
                                <div className="row">
                                    <div className="box">C</div>
                                    <div className="box">D</div>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="big-box">E</div>
                            </div>
                        </div> :
                        <div>
                            <div className="box">A</div>
                            <div className="box">B</div>
                            <div className="box">E</div>
                            <div className="box">C</div>
                            <div className="box">D</div>
                        </div>
                }
            </div>
        )
    }
}

export default Dashboard;
