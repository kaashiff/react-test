import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from "./Dashboard/Dashboard";
import Login from "./Login/Login";
import Register from "./Register/Register";

class App extends Component {
    render() {

        return (
            <div className="app-routes">
                <Switch>
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                    <Route path="/" component={Dashboard} />
                </Switch>
            </div>
        );
    }
}

export default App;
