This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

I have used https://reqres.in API for login, register and to list out all users.

To register or login an user use these credentials:

### Email : eve.holt@reqres.in

### Password : pistol
    
Google Login will work on only [http://localhost:3000](http://localhost:3000) origin.
